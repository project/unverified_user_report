<?php

/**
 * @file
 * The unverified user report template.
 *
 * This is designed to work with a plaintext email, so we're avoiding a table.
 *
 * Includes:
 *  - $is_empty - Whether or not we have anything to report.
 *  - $users - An array of users to report.
 *  - $rows - An array of records with uid, created, ago, and mail.
 *  - $count - The number of users in the report.
 */
?>
<?php if ($is_empty) { ?>
<?php print t('No unverified users to report!'); ?>
<?php } else { ?>
<?php print $count; ?> <?php print t('Unverified Users (people that have registered but never logged in.)'); ?><br><br>

<?php foreach ($rows as $row): ?>
 - <?php print $row['mail']; ?> (<?php print t('Registered'); ?> <?php print $row['ago']; ?>)<br>
<?php endforeach; ?>
<?php } ?>
