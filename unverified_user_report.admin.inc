<?php

/**
 * @file
 * Admin page callbacks for the unverified_user_report module.
 */

/**
 * Form constructor for the main unverified_user_report administration form.
 */
function unverified_user_report_settings_form($form, &$form_state) {
  $last_sent = cache_get('unverified_user_report_last_sent');
  if (is_object($last_sent) && !empty($last_sent->data)) {
    $last_sent = $last_sent->data;
  }
  else {
    $last_sent = time();
    cache_set('unverified_user_report_last_sent', $last_sent);
  }

  $form['header'] = array(
    'header' => array(
      '#prefix' => '<h1>',
      '#markup' => t('Unverified User Report Settings'),
      '#suffix' => '</h1>',
    ),
    'description' => array(
      '#prefix' => '<p>',
      '#markup' => t('The settings below control when and to whom the reports
                      get sent.<br><br><strong>The last run time</strong><br>
                      @last_time (!reset)<br><br><strong>The next time a report will be
                      sent</strong><br>@next_time',
        array(
          '@last_time' => date("Y F d h:i a T", $last_sent),
          '!reset' => l(t('reset'), 'admin/config/system/unverified_user_report/reset-run-time'),
          '@next_time' => date("Y F d h:i a T", ($last_sent + variable_get('unverified_user_report_interval', (60 * 60 * 24 * 7)))),
        )
      ),
      '#suffix' => '</p>',
    ),
  );
  $form['unverified_user_report_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval'),
    '#default_value' => variable_get('unverified_user_report_interval', (60 * 60 * 24 * 7)),
    '#description' => t('Interval between reports being sent. Defaults to once a week.'),
  );
  $form['unverified_user_report_emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Emails'),
    '#default_value' => variable_get('unverified_user_report_emails', NULL),
    '#description' => t('Format just like you would for a "to:" line in an email
                         program. Use commas to separate email addresses.'),
  );
  $form['unverified_user_report_age_of_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Age of Account'),
    '#default_value' => variable_get('unverified_user_report_age_of_account', (60 * 60 * 24 * 7)),
    '#description' => t('You can filter the automated report by only showing
                         accounts that are so many seconds old. Defaults to one
                         week.'),
  );
  $form['unverified_user_report_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#default_value' => variable_get('unverified_user_report_limit', 50),
    '#description' => t('Limit the number of users to report on. Default is 50,
                         you could go as high as 10,000 before it becomes a
                         limiting factor.'),
  );

  return system_settings_form($form);
}

/**
 * Menu callback to reset the run time.
 */
function unverified_user_report_reset_run_time() {
  $last_sent = time();
  $send_next_cron = $last_sent - variable_get('unverified_user_report_interval', (60 * 60 * 24 * 7)) - 1;
  cache_set('unverified_user_report_last_sent', $send_next_cron);
  drupal_set_message(t('The Unverified User Report Run time has been reset so
                        that it will run in the next cron.'));
  drupal_goto('admin/config/system/unverified_user_report/overview');
}
