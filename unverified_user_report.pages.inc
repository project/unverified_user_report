<?php

/**
 * @file
 * Page callbacks for unverified_user_report module.
 */

/**
 * Menu callback; presents main Unverified User Report: Live Report page.
 */
function unverified_user_report_main_page() {
  $users = unverified_user_report_generate();
  $build['header'] = array(
    '#markup' => '<h1>' . t('Unverified Users Report') . '</h1><hr><br><br>',
  );
  $build['report'] = array(
    '#theme' => 'unverified_user_report_list',
    '#users' => $users,
  );
  return $build;
}
